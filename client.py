#!/usr/bin/python3
import logging

from bots.create_bot import CreateBot
from helpers.helper import Helper
from utils.cgi import ClientGatewayInterface
from utils.client import Client
from utils.name_server import NameServer

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(levelname)s][%(filename)s][%(funcName)s:%(lineno)d]: %(message)s"
)
LOGGER = logging.getLogger(__name__)


class ClientExecutor(object):

    def __init__(self, builder):
        self.name = builder.name
        self.server_name = builder.server_name
        self.local_host = builder.local_host
        self.local_port = builder.local_port
        self.server_host = builder.server_host
        self.server_port = builder.server_port

    def start(self):
        with NameServer(host=self.local_host, port=self.local_port) as name_server:
            sgi = Helper.pyro(self.server_host, self.server_port).get_server(self.server_name)
            if sgi:
                id = sgi.register(self.name, self.local_host, self.local_port)
                if id:
                    with Client(name=self.name, sgi=sgi) as client:
                        LOGGER.info("Registered client. Got an ID: %s", id)
                        client.set_id(id)
                        cgi = ClientGatewayInterface(name_server=name_server, client=client)
                        cgi.start()
                        bot = CreateBot.builder(client, self.name).build()
                        bot.start()
                else:
                    LOGGER.warning("Could not register the client.")
            else:
                LOGGER.warning("Could not get remote server gateway interface (sgi).")
        LOGGER.warning("Exit")

    @classmethod
    def builder(cls):
        return cls._Builder()

    class _Builder(object):

        def __init__(self):
            self.name = None
            self.server_name = None
            self.local_host = None
            self.local_port = None
            self.server_host = None
            self.server_port = None

        def _name(self):
            default = "client"
            msg = "Write your client's name [{}]: ".format(default)
            return input(msg) or default

        def _local_host(self):
            default = Helper.ip().get_ip_address()
            msg = "Write your public ip for connection [{}]: ".format(default)
            return input(msg) or default

        def _local_port(self):
            default = 9090
            msg = "Write port for your Pyro4 object [{}]: ".format(default)
            return input(msg) or default

        def _server_name(self):
            default = "server"
            msg = "Write server's name for connection [{}]: ".format(default)
            return input(msg) or default

        def _server_host(self):
            default = Helper.ip().get_ip_address()
            msg = "Write server ip for connection [{}]: ".format(default)
            return input(msg) or default

        def _server_port(self):
            default = 9091
            msg = "Write server port [{}]: ".format(default)
            return input(msg) or default

        def build(self):
            self.name = self._name()
            self.server_name = self._server_name()
            self.local_host = self._local_host()
            self.local_port = int(self._local_port())
            self.server_host = self._server_host()
            self.server_port = int(self._server_port())

            return ClientExecutor(self)


if __name__ == '__main__':
    LOGGER.info("Starting client!")
    instance = ClientExecutor.builder().build()
    instance.start()
