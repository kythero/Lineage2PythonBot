# Lineage 2 Bot with VirtualMachines

Bot has been created for Lineage 2 Game. It may act like a clicker in basic mode.
Advanced mode provides connection to VirtualMachines when other character-bots are logged in.
Script allows communicate data between all virtualMachines and bots on them. Connection is provided by Pyro4.

## Warning!

Bots are under tests. FortuneSeeker acts like a servitor. ... -> follow -> assist -> spoil/attack -> kill/sweep -> follow -> ...
@Tests ongoing on Lineage 2 Classic 2.9 Secret of Empire.

@Tested Interlude (be sure that your client has enter-mode chat) (with version 3.0.0+ it's not necessary anymore.)

```bash
@Beta 3.3.5
Python version 3.5
Warning: module is under reconstruction - 12.08.2019 (!)
```

## Getting Started

Code takes care on objects and handle connection, but core elements like ready VirtualMachines with the network setup is a different part.
In most cases basic installation is enough. In my setup I use Debian 8, for running server.py, and then in every single VM (Windows 7) where Lineage2 is running, only client.py is executed.
Server can be started on Windows aswell, if you are not familiar with linux.

### server

Server is a handler for all connected clients to him. Each client send his own data to the server (client's data is a HP, MP, CP, target etc).
Server stores all data, merges it, and resends to all registered clients. In this case, clients have full knowledge about all parameters from others.
Client X will know what Client Y targeted, how many HP this target has, and so on.

### client

Client is a bot. During creation you pick the class and according to choice, it will have different logic. 
Cardinal for example, will force to heal all clients.
Spoilers, will always follow you, assist you, and spoil/sweep the target.
There is possibility to take class "player", which will not have automatic logic, but allows you to use auto-potions and create some handy shortcuts - especially to communicate with other clients-bots.
With good configuration, you may be like a summoner with party of servitors.

## How does it work?

In quick way, most gameGuards is like a "sandbox" for the Lineage2 game. In most cases, when you want to send "buttons/keys" to the window, it will not allow you to do that.
The script is not a L2Tower or Adrenaline, where you create bypass, run your game and go to school/work. 
The script simulates human behavior through the VirtualMachine.

This solution is for players who loves this game, like raids, love spoil but they do not have friends! :(

## How to Install

Project contains git submodules. If you do not use **--recursive**, update submodules after repository is cloned.

```bash
git clone --recursive https://github.com/marekgancarz/Lineage2PythonBot.git
```

```python
pip install statistics Pyro4 requests psutil keyboard
```

## Changes

**Patch 3.3.5**:
* rewrite Cardinal and Fortune Seeker with new libaries. Fortune Seeker has been tested, and it works with normal warror mode with spoil/sweep,
* implement Cardinal options, but it still needs investigation. Thinking about the strategy, about different character levels.
* Basic Summoner class, with follow mode.
* AdvancedController allows execute dual-boxes, and handle bots without VirtualMachines. Only exception is, if server does not have hard gameGuards which will block postMessageA
* Cardinal heals only clients. (bots connected together with the player)
* Fortune seeker with basic mode works. Acts like a servitor with auto follow, assist, attack, spoil and sweep.
* Reactions are decent. Still need improve bot's decisions about assisting/following. 

**Patch 3.2.2**:
* moved memory scripts into external memoryScanner. 
* Improved memory read - search values through the memory chunks and works with bytes.
* removed dummy calculations through the offsets.
* first tests clients and server through the VMs passed. Sgi and Cgi cooperate without issues.

**Patch 3.1.0**:
* implement ServerGatewayInterface (**sgi**) and ClientGatewayInterface (**cgi**). Those interfaces are registered in Pyro4.
* client and server now are independent instances, working in their own Threads. Connections to them is possible through the **sgi** and **cgi**.
* during connection, client request the ID (token) from server. When connection is established, wrong clients, 
or clients without an ID will be ignored/removed.
* added more helpers to handle custom configuration - more default/ready values.

**Patch 3.0.0**:
* include server.py, working on Unix
* include client.py, working only on Windows
* refactored helpers and catalog structure


**Patch 2.0.0**:
* rewrite version 1.0.0 for python 3.5
* include *Expectancy* module, and *concurrent.futures.ThreadPoolExecutor*

## Authors

* **Marek Gancarz** - *Initial work* - [marekgancarz](https://github.com/marekgancarz)

## Features

* Handle connection over virtualMachines.
* two modes to execute commands to the window:
    1) postMessageA - code does not send hotkeys, but use the commands  like: /useskill wind strike, or /trade, /invite directly to the game window.
    **warning**: check before use if game's gameGuard will not filter messages.
    2) keyboard_inputs - code sends fake keystrokes directly to the window. GameGuard will not even try filter it. Window with game must be active -> use VirtualMachine (VMWare)
    **warning**: if your game cannot be executed in VirtualMachine (some gameGuard checks it aswell) you will not find benefits from this script.
