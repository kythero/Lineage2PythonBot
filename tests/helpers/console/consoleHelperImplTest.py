import unittest
from app.helpers.helper import Helper


class ConsoleHelperImplTest(unittest.TestCase):

    def test_insert_correct(self):
        # given
        key = 'process_name'
        value = 'l2.bin'
        settings = {key: value}

        # when
        result = Helper.console().insert(key, settings)

        # then
        self.assertIn(key, settings)
        self.assertEqual(result, value)

    def test_insert_no_storage(self):
        # given
        key = 'process_name'
        settings = None

        # when
        result = Helper.console().insert(key, settings)

        # then
        self.assertIn(key, settings)
        self.assertTrue(isinstance(settings, dict))

    def test_insert_no_key(self):
        # given
        key = None
        settings = None

        # when
        result = Helper.console().insert(key, settings)

        # then
        self.assertTrue(isinstance(settings, dict))
        self.assertEqual(len(result), 1)
