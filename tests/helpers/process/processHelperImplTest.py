import unittest
from unittest.mock import patch, Mock

import psutil

from app.helpers.helper import Helper
from app.helpers.process.processHelperImpl import ProcessHelperImpl


class ProcessHelperImplTest(unittest.TestCase):

    helperAbc = patch('app.helpers.helperImpl.HelperAbc')

    @classmethod
    def setUpClass(cls):
        cls.helperAbc.process = Mock(return_value=ProcessHelperImpl())
        Helper.set_impl(cls.helperAbc)

    def test_get_pid_correct(self):
        # given
        process_list = list(psutil.process_iter())

        # when
        process_name = process_list[0].name()
        pid = process_list[0].pid

        # then
        result = Helper.process().get_pid(process_name)
        self.assertEqual(result, pid)

    def test_get_pid_wrong_name(self):
        # given
        process_name = "NoExist"

        # when

        # then
        result = Helper.process().get_pid(process_name)
        self.assertNotEqual(result, 0)



if __name__ == '__main__':
    unittest.main()