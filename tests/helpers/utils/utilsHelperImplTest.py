import unittest
from app.helpers.helper import Helper

class UtilsHelperImplTest(unittest.TestCase):

    def save(self):
        pass

    def load(self):
        pass

    def test_hex(self):
        # given
        value_as_string = "0x80808080"

        # when
        result = int(value_as_string, 16)

        # then
        value = Helper.utils().hex(value_as_string)

        self.assertEqual(result, value)


if __name__ == '__main__':
    unittest.main()