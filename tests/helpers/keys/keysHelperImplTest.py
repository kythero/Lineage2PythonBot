import unittest
from app.helpers.helper import Helper


class KeysHelperImplTest(unittest.TestCase):

    def test_mapper(self):
        # given
        message = "hello{ENTER}"

        # when
        results = Helper.keys().mapper(message)

        # then
        self.assertIn("ENTER", results)
        self.assertEqual(len(results), 6)

    def test_mapper_format(self):
        # given
        user = "john"
        message = "hello{{SPACE}}{user}".format(user=user)

        # when
        results = Helper.keys().mapper(message)

        # then
        self.assertEqual(len(results), 10)
        self.assertIn("SPACE", results)

    def test_mapper_format_space(self):
        # given
        user = "john"
        message = "hello {user}".format(user=user)

        # when
        results = Helper.keys().mapper(message)

        # then
        self.assertEqual(len(results), 10)
        self.assertIn(" ", results)


if __name__ == '__main__':
    unittest.main()
