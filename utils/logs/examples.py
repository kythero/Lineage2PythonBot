#!/usr/bin/python3.5
from table_logger import TableLogger


if __name__ == '__main__':
    table = TableLogger("test", "test-1", "col-2", "col-4")
    table.add_row("House", "building", "big city!")
    table.add_row("Warsaw", "Gaming", "no1")
    table.add_row("error", "buff me", "Separator")
    print(table.get_table())
    print(table)
