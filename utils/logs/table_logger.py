#!/usr/bin/python3.5
import logging

LOGGER = logging.getLogger(__name__)

class TableLogger(object):

    NEW_LINE = '\r\n'
    OFFSET = 2
    R_SEPARATOR = '='
    C_SEPARATOR = '|'

    def __init__(self, table_name="Unknown", *columns):
        self.name = table_name
        self.columns = columns
        self.widths = [len(content) + self.OFFSET for content in self.columns]
        self.rows = []
        self._sort = None
        self.footer = ''

    def add_row(self, *values):
        if not len(values) == len(self.columns):
            raise ValueError("Values [{}] do not fit declared columns [{}].".format(values, self.columns))

        for index, value in enumerate(values):
            new_length = len(str(value)) + self.OFFSET
            if new_length > self.widths[index]:
                self.widths[index] = new_length
        self.rows.append(values)

    def get_table(self):
        return self.__str__()

    def sort_by(self, column_name):
        self._sort = column_name

    def add_footer(self, sentence):
        self.footer = sentence

    def __str__(self):
        record = "  {{: <{}}}%s " % (self.C_SEPARATOR,)
        records = self.C_SEPARATOR + record * len(self.columns)
        separator_length = sum(self.widths) + 4*len(self.columns)

        row_string = records.format(*self.widths)

        start = self.R_SEPARATOR * 3
        start += ' Title: "{}" '.format(self.name)
        end = self.R_SEPARATOR * (separator_length - len(start))
        start += end

        table = self.NEW_LINE
        table += start
        table += self.NEW_LINE
        table += row_string.format(*self.columns)
        table += self.NEW_LINE
        table += '=' * (sum(self.widths) + 4*len(self.columns))
        table += self.NEW_LINE

        try:
            if self._sort:
                column_index = self.columns.index(self._sort)
                self.rows.sort(key=lambda x: x[column_index])
        except ValueError as ex:
            LOGGER.error("Could not sort table with given column name {}. "
                         "Available columns: {}. Exception: {}.".format(self._sort, self.columns, ex))

        for row in self.rows:
            table += row_string.format(*row)
            table += self.NEW_LINE
        if self.footer:
            table += self.footer
            table += self.NEW_LINE

        return table
