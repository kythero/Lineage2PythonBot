#!/usr/bin/python3
import threading
import logging
import time
from helpers.helper import Helper
from concurrent.futures import ThreadPoolExecutor
LOGGER = logging.getLogger(__name__)


class Server(threading.Thread):
    """
    Server with its own logic. Access to it is via ServerGatewayInterface (sgi).
    """

    WORKERS = 3
    SERVER_DELAY = 0.1  # seconds
    CLIENTS = dict()
    DATA = dict()
    STATUS = True
    RESYNC = False

    def __init__(self, name="Default"):
        super(Server, self).__init__()
        self.name = name
        self.setDaemon(True)
        self.start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def update(self):
        """
        main method which allow define logic for the server.

        :return: None
        """
        if self.RESYNC:
            with ThreadPoolExecutor(max_workers=self.WORKERS) as executor:
                for client_id, client in self.CLIENTS.copy().items():
                    executor.submit(self.synchronize, client_id, client)
            self.RESYNC = False

    def run(self):
        LOGGER.info("Starting server with name: '%s'.", self.name)
        while True:
            self.update()
            time.sleep(self.SERVER_DELAY)

    def register(self, id, client):
        """
        Register given client with id.

        :param id: generated client's id during registration.
        :param client: tuple, client's name, host and port.
        :return: True if registered, False otherwise.
        """
        if self.CLIENTS.get(id, None):
            return False
        else:
            self.CLIENTS[id] = client
            self.RESYNC = True
            return True

    def unregister(self, id):
        """
        Unregister client with given id from server.

        :param id: client's id.
        :return: True if unregister, False otherwise.
        """
        if id in self.CLIENTS:
            del self.CLIENTS[id]
            return True
        return False

    def synchronize(self, id, client):
        """
        Synchronize data from server to the client.

        :param id: client's id
        :param client: tuple, client's name, host and port
        :return: None
        """
        client_name, client_host, client_port = client
        cgi = Helper.pyro(client_host, client_port).get_server(client_name)
        if cgi:
            LOGGER.info("Synchronize client with name [%s] under [%s:%s].", *client)
            data_state = cgi.synchronize_data(id, self.DATA)
            status_state = cgi.synchronize_status(id, self.STATUS)
            if not (data_state and status_state):
                LOGGER.warning("Id for client is wrong. Removing it.")
                self.unregister(id)
        else:
            LOGGER.warning("Could not connect to client with name [%s] under [%s:%s]. Removing it from registry.", *client)
            self.unregister(id)

    def upload_data(self, remote_data):
        """
        Update stored data on server side from given remote client.

        :param remote_data: remote data delivered from client.
        :return: None
        """
        old_data = self.DATA.copy()
        self.DATA.update(remote_data)
        if not old_data == self.DATA:
            self.RESYNC = True

    def pause(self):
        """
        Set Pause state. Server during synchronization will set all clients in IDLE state.

        :return: Boolean. True if clients should work. False otherwise.
        """
        self.STATUS = not self.STATUS
        if self.STATUS:
            LOGGER.info("Server started again.")
        else:
            LOGGER.info("Server has been paused.")
        return self.STATUS
