

def default_value(value, expected_exceptions):
    """
    Wrapper for methods. Usable if your method has decorator: @property.
    Allow define default value, when expected_exceptions occur.

    :param value: default value
    :param expected_exceptions: single exception or tuple with many.
    :return: wrapper function response or default value.
    """
    _value = value
    _expected_exceptions = expected_exceptions
    def wrap_function(func):
        def wrap_method(self, *args, **kwargs):
            try:
                response = func(self, *args, **kwargs)
            except _expected_exceptions:
                response = _value
            return response
        return wrap_method
    return wrap_function