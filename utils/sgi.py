#!/usr/bin/python3
import logging
import Pyro4
import random

LOGGER = logging.getLogger(__name__)

@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class ServerGatewayInterface(object):
    """
    Gateway to our server. It allows register, unregister and use possible methods to interact with a server.
    Sgi is registered in Pyro4.
    """

    def __init__(self, name_server=None, server=None):
        self.name_server = name_server
        self.server = server
        self.name = self.server.name
        self.ns = self.name_server.ns
        self.host = self.name_server.host
        self.port = self.name_server.port
        self.uri = None

    def start(self):
        """
        Create Pyro4's daemon and execute its loop.

        :return: None
        """
        LOGGER.info("Starting Pyro4's daemon with name [%s] under [%s:%s].", self.name, self.host, self.port)
        with Pyro4.Daemon(host=self.host) as daemon:
            self.uri = daemon.register(self)
            self.ns.register(self.name, self.uri)
            daemon.requestLoop()

    def register(self, client_name, client_host, client_port):
        """
        Register client in server.

        :param client_name: client's name
        :param client_host: client's ip
        :param client_port: client's port
        :return: id if register, False otherwise
        """
        id = self._generate_id()
        client = (client_name, client_host, client_port)
        registered = self.server.register(id, client)
        if registered:
            return id
        else:
            LOGGER.warning("Could not register given client %s with %s:%s", *client)
        return False

    def unregister(self, id):
        """
        Unregister client from server.

        :param id:  client's id.
        :return: True if unregistered, False otherwise.
        """
        return self.server.unregister(id)

    def upload_data(self, id, remote_data):
        """
        Upload data from remote clients into the server.

        :param id: client's id. Id generated during client registration.
        :param remote_data: dict. Data from client with key is a bot/character name and values is a dict with parameters.
        :return: True if uploaded, False otherwise.
        """
        if self.check_if_registered(id):
            self.server.upload_data(remote_data)
            return True
        else:
            LOGGER.info("You are not registered!")
        return False

    def check_if_registered(self, id):
        """
        Check if given ID is registered.

        :param id: client's id
        :return: True if registered, False otherwise.
        """
        return (id in self.server.CLIENTS.keys())

    def _generate_id(self):
        id = random.randint(1, 101)
        while self.check_if_registered(id):
            id = random.randint(1, 101)
        return id
