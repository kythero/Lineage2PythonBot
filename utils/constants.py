
CURRENT_HP = 'currentHP'
MAX_HP = 'maxHP'
VALUE = 'value'
MEMORY_ADDRESS = 'memory_address'

CURRENT_TARGET_HP = 'currentTargetHP'
MAX_TARGET_HP = 'maxTargetHP'
GREMLIN_HP = 97
CURRENT_MP = 'currentMP'
MAX_MP = 'maxMP'

FILE_SETTINGS = "settings.json"
FILE_LEARN_DATA = "learn_data.json"
FILE_MEMORY_DATA = "memory_data.json"