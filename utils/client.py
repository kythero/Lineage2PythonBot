#!/usr/bin/python3
import threading
import time
import logging

LOGGER = logging.getLogger(__name__)

class Client(threading.Thread):
    """
    Client thread to store data. Data is updated from server if its running and client has directly communication
    with running bot. Remote user/server has access to the Client via ClientGatewayInterface (cgi).
    """

    DELAY = 1  # second

    DATA = dict()
    STATUS = None

    def __init__(self, name="Default", sgi=None):
        super().__init__()
        self.setDaemon(True)
        self.id = None
        self.name = name
        self.sgi = sgi
        self.start()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def update(self):
        """
        Main method to include client's logic.

        :return: None
        """
        pass

    def set_id(self, id):
        """
        setter for client's id.

        :param id: registered client's id, given from sgi.
        :return:
        """
        self.id = id

    def run(self):
        while True:
            self.update()
            time.sleep(self.DELAY)

    def synchronize_data(self, remote_data):
        """
        Synchronize data given from server.

        :param remote_data: server's data.
        :return: None
        """
        self.DATA = remote_data

    def synchronize_status(self, remote_status):
        """
        Synchronize status given from server.

        :param remote_status: server's status. It's a pause. If status is true, client will be in state IDLE.
        :return: None
        """
        self.STATUS = remote_status
