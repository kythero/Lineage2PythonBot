#!/usr/bin/python3
import threading
from Pyro4 import naming, locateNS
import logging
import time

LOGGER = logging.getLogger(__name__)


class NameServer(threading.Thread):
    """
    This class allows execute Pyro4's nameServer in thread.
    When NameServer is ready, objects can be stored.
    https://pythonhosted.org/Pyro4/nameserver.html
    """
    DELAY = 2  # seconds

    def __init__(self, host="127.0.0.1", port=9090):
        super().__init__()
        self.setDaemon(True)
        self.host = host
        self.port = int(port)
        self.ns = None
        self.start()
        time.sleep(self.DELAY)

    def __enter__(self):
        self.ns = locateNS(host=self.host, port=self.port)
        LOGGER.debug("Found NameServer: %s", self.ns)
        return self

    def __exit__(self, *args):
        pass

    def run(self):
        LOGGER.debug("Started NameServer on [%s:%s]", self.host, self.port)
        naming.startNSloop(host=self.host, port=self.port)
