#!/usr/bin/python3
import Pyro4
import logging
import threading

LOGGER = logging.getLogger(__name__)

@Pyro4.expose
@Pyro4.behavior(instance_mode="single")
class ClientGatewayInterface(threading.Thread):
    """
    Client gateway interface (cgi) is a bridge between user/server and running client. Cgi is registered in Pyro4.
    """

    def __init__(self, name_server=None, client=None):
        super(ClientGatewayInterface, self).__init__()
        self.setDaemon(True)
        self.name_server = name_server
        self.client = client
        self.id = self.client.id
        self.name = self.client.name
        self.sgi = self.client.sgi
        self.ns = self.name_server.ns
        self.host = self.name_server.host
        self.port = self.name_server.port
        self.uri = None

    def run(self):
        with Pyro4.Daemon(host=self.host) as daemon:
            self.uri = daemon.register(self)
            self.ns.register(self.name, self.uri)
            daemon.requestLoop()

    def synchronize_data(self, id, data):
        """
        Synchronize given remote data with specific id.

        :param id: registered client's id.
        :param data: remote data
        :return: True if synchronized, False otherwise.
        """
        if id == self.id:
            self.client.synchronize_data(data)
            return True
        else:
            LOGGER.info("Could not synch data. Id are not the same!")
        return False

    def synchronize_status(self, id, remote_status):
        """
        Synchronize given remote status with specific id.

        :param id: registered client's id.
        :param data: remote status
        :return: True if synchronized, False otherwise.
        """
        if id == self.id:
            self.client.synchronize_status(remote_status)
            return True
        else:
            LOGGER.info("Could not synchronize status. Id are not the same!")
        return False
