from .pyro_helper import PyroHelper
from .bot_helper import BotsHelper
from .file_helper import FileHelper
from .memory_helper import MemoryHelper
from .utils_helper import UtilsHelper
from .keys_helper import KeysHelper
from .console_helper import ConsoleHelper
from .process_helper import ProcessHelper
from .ip_helper import IpHelper


class Helper(object):

    @classmethod
    def pyro(cls, host=None, port=None):
        return PyroHelper(host, port)

    @classmethod
    def bots(cls):
        return BotsHelper

    @classmethod
    def utils(cls):
        return UtilsHelper

    @classmethod
    def file(cls):
        return FileHelper

    @classmethod
    def console(cls):
        return ConsoleHelper

    @classmethod
    def keys(cls):
        return KeysHelper

    @classmethod
    def memory(cls, memory, chunks):
        return MemoryHelper(memory, chunks)

    @classmethod
    def process(cls):
        return ProcessHelper

    @classmethod
    def ip(cls):
        return IpHelper
