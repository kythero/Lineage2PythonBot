import json
import os
import logging

LOGGER = logging.getLogger(__name__)


class FileHelper(object):
    """
    Helper for handling files.
    """

    HOME = os.path.expanduser('~')
    CATALOG_NAME = '.hidden_one'
    CATALOG_PATH = os.path.join(HOME, CATALOG_NAME)

    def __new__(cls, *args, **kwargs):
        cls.__create_hidden_dir()

    @classmethod
    def load(cls, name):
        """
        Load file from json extension. All files are stored in hidden_dir.

        :param name: file's name.
        :return: file content, False otherwise.
        """
        cls.__create_hidden_dir()
        file_name, file_path = cls._validate_json_file(name)
        try:
            with open(file_path, 'r') as json_file:
                return json.load(json_file)
        except IOError:
            LOGGER.debug("Could not find file %s under given path %s.", file_name, file_path)
        return False

    @classmethod
    def save(cls, name, data):
        """
        Load data into file with json extension. All files are stored in hidden_dir.

        :param name: file's name.
        :param data: stored data, dictionary.
        :return: file content, False otherwise.
        """
        cls.__create_hidden_dir()
        file_name, file_path = cls._validate_json_file(name)
        try:
            with open(file_path, 'w') as json_file:
                json.dump(data, json_file)
            return True
        except IOError:
            LOGGER.debug("Could not store data %s into file %s, under its path: %s.", data, file_name, file_path)
        return False

    @classmethod
    def _validate_json_file(cls, name):
        """
        Validate file if it has json extension.

        :param name: file name
        :return: tuple, with file name and it's path
        """
        file_name = name if name.endswith('.json') else "{}.json".format(name)
        return file_name, os.path.join(cls.CATALOG_PATH, file_name)

    @classmethod
    def __create_hidden_dir(cls):
        if not os.path.exists(cls.CATALOG_PATH):
            os.makedirs(cls.CATALOG_PATH, mode=0o755, exist_ok=False)
            LOGGER.debug("Hidden dir has been created. All saved configuration will be stored there.")
