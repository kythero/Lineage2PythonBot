import logging
import itertools
from .file_helper import FileHelper
from utils.constants import FILE_MEMORY_DATA

LOGGER = logging.getLogger(__name__)


class UtilsHelper(object):

    @classmethod
    def combinations(cls, pure=False):
        """
        1) pure=True: use file with data, how many times letters in our memory_address accured.
        2) counts how many times letter occured in previous ram scan.
        3) build scenarios. The list with tuples, where tuple(0) contain index: number of character in our string. example: "0x1D8E02F8", index:0 == 1, index:1 == D etc.
           tuple(1) - list of keys from our possibilities, sorted by values (number of occurs)
           tuple(2) - weigh - how many times changes occured in one row
        4) sort scenarios by weigh (tuple(3) from previous step) from lowest occur to highest
        5) build list of tuples where tuple(0) - index, and tuple(1) the letter
        6) use itertools.product to build combinations
        7) as a result we have unsorted, mixed combination. Sort now by index.
        8) build string from given sorted list and yield result.

        :param pure:
        :return:
        """
        possibles = {
            '0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '6': 0, '7': 0, '8': 0, '9': 0,
            'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0
        }
        setups = [
            {'0': 0, '1': 0, '2': 0},
            dict(possibles),
            dict(possibles),
            dict(possibles),
            dict(possibles),
            dict(possibles),
            dict(possibles),
            {'8': 0}
        ]
        if not pure:
            memory_data = FileHelper.load(FILE_MEMORY_DATA)

            if memory_data:
                address_list = memory_data.get("currentHP", [])
                LOGGER.debug(address_list)

                for address in address_list:
                    letters = address.replace('X', 'x').split("x")[1]
                    for index, letter in enumerate(letters):
                        setups[index][letter.upper()] += 1

        scenarios = [
            (index,
             sorted(setup, key=setup.get, reverse=True),
             len([v for v in setup.values() if v > 0])) for index, setup in enumerate(setups)
        ]

        scenarios.sort(key=lambda x: x[2])

        scenario_2 = []
        for scenario in scenarios:
            index, elements, _ = scenario
            scenario_2.append([(index, element) for element in elements])

        for scenario in itertools.product(*scenario_2):
            decode = sorted(scenario, key=lambda x: x[0])
            yield "0x" + "".join([letter for _, letter in decode])
