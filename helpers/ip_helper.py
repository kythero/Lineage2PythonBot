import socket


class IpHelper(object):

    @classmethod
    def get_ip_address(cls):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]
