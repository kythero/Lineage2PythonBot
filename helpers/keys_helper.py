from utils.EventController.inputs import keysConstants

class KeysHelper(object):
    """
    Class Helper for Keys.
    """
    # LINK: msdn.microsoft.com/en-us/library/dd375731
    KEYS = keysConstants.KEYS