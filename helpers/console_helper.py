

class ConsoleHelper(object):
    """
    Helper for handle console.
    """

    @classmethod
    def insert(cls, key, settings, message=""):
        if not isinstance(settings, dict):
            settings = dict()

        if key in settings:
            element = input("{0} [{1}:{2}]: ".format(message, key, settings[key])) or settings[key]
        else:
            element = input("{0} {0}: ".format(message, key))

        settings[key] = element
        return element
