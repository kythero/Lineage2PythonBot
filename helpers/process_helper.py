import logging
import psutil

LOGGER = logging.getLogger(__name__)


class ProcessHelper(object):

    @classmethod
    def get_pid(cls, process_name):
        pids = []
        for process in psutil.process_iter():
            if process.name().lower() == process_name.lower():
                pids.append(int(process.pid))
        if len(pids) == 1:
            return pids[0]
        else:
            return pids

    def process_exists(self, pid):
        for process in psutil.process_iter():
            if process.pid == pid:
                return True
        return False
