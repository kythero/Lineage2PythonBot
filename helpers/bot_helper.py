from utils.constants import *


class BotsHelper(object):

    @classmethod
    def validate_stored_data(self, pid, storage, name):
        if storage.get(name, None):
            with Memory(pid=pid) as memory:
                hexed_currentHP = int(storage[name][CURRENT_HP][MEMORY_ADDRESS], 16)
                hexed_maxHP = int(storage[name][MAX_HP][MEMORY_ADDRESS], 16)

                hexed_currentMP = int(storage[name][CURRENT_MP][MEMORY_ADDRESS], 16)
                hexed_maxMP = int(storage[name][MAX_MP][MEMORY_ADDRESS], 16)

                hexed_current_target_HP = int(storage[name][CURRENT_TARGET_HP][MEMORY_ADDRESS], 16)
                hexed_max_target_HP = int(storage[name][MAX_TARGET_HP][MEMORY_ADDRESS], 16)

                value_currentHP = memory.read_memory(hexed_currentHP)
                value_maxHP = memory.read_memory(hexed_maxHP)

                value_currentMP = memory.read_memory(hexed_currentMP)
                value_maxMP = memory.read_memory(hexed_maxMP)

                value_currentTargetHP = memory.read_memory(hexed_current_target_HP)
                value_maxTargetHP = memory.read_memory(hexed_max_target_HP)

                if (value_currentHP == storage[name][CURRENT_HP][VALUE] and
                        value_maxHP == storage[name][MAX_HP][VALUE] and
                        value_currentMP == storage[name][CURRENT_MP][VALUE] and
                        value_maxMP == storage[name][MAX_MP][VALUE] and
                        value_currentTargetHP == storage[name][CURRENT_TARGET_HP][VALUE] and
                        value_maxTargetHP == storage[name][MAX_TARGET_HP][VALUE]):
                    return True
        return False
