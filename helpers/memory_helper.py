import logging
import re
from utils.memoryScanner.utils.targets import IntValue

LOGGER = logging.getLogger(__name__)

class MemoryHelper(object):

    def __init__(self, memory, chunks):
        self.memory = memory
        self.chunks = chunks

    def get_target_health_memory_address(self,expected_hp, choosen_chunk=None):
        """
        To get handlers for target max_hp and current_hp during analize, target yourself. Target hp is in different location.
        Memory for target hp has switched max<->current hp order, than for characters.

        :param expected_hp: the easiest, your hp and target yourself.
        :return: list of tuples, where list[0] is a max_hp, and list[1] is a current_hp
        """
        int_value = IntValue(int(expected_hp))
        results = []
        selected_chunk = None
        if choosen_chunk:
            choosen_chunk_base_address, _ = choosen_chunk
        else:
            choosen_chunk_base_address = 0
        for chunk in self.chunks:
            base_address, _ = chunk
            chunk_bytes = self.memory.get_chunk_bytes(chunk)
            if int_value.bytes in chunk_bytes:
                try:
                    offsets = [m.start() for m in re.finditer(int_value.bytes, chunk_bytes)]
                    if offsets and (len(offsets) == 2 and offsets[0] % 8 == 0 and
                            offsets[1] % 4 == 0 and
                            abs(offsets[0] - offsets[1]) == 4) and choosen_chunk_base_address and abs(base_address - choosen_chunk_base_address) < 2000000:
                            for offset in offsets:
                                selected_chunk = chunk
                                results.append((hex(base_address + offset), int_value.value))
                            break
                except re.error as ex:
                    LOGGER.debug("Could not scan chunks for target_health. Exception %s", ex)
        current_hp, max_hp = results[0], results[1]
        return max_hp, current_hp, selected_chunk

    def get_health_memory_address(self, expected_hp):
        """
        Get List of tuples, where tuple[0] is a current hp and tuple[1] is a max hp.
        each tuple has inside memory_address and startable value.

        :param expected_hp: your hp. To proper work, your max nad current hp should be equal.
        :return: list of tuples, where list[0] is a max_hp, and list[1] is a current_hp
        """
        int_value = IntValue(int(expected_hp))
        results = []
        selected_chunk = None
        for chunk in self.chunks:
            base_address, _ = chunk
            chunk_bytes = self.memory.get_chunk_bytes(chunk)
            if int_value.bytes in chunk_bytes:
                try:
                    offsets = [m.start() for m in re.finditer(int_value.bytes, chunk_bytes)]
                    if offsets and len(offsets) == 2 and offsets[0] % 8 == 0 and offsets[1] % 8 == 0 and abs(offsets[0] - offsets[1]) == 8:
                        for offset in offsets:
                            selected_chunk = chunk
                            results.append((hex(base_address + offset), int_value.value))
                        break
                except re.error as ex:
                    LOGGER.debug("Could not scan chunks for health. Exception %s", ex)
        max_hp, current_hp = results[0], results[1]
        return max_hp, current_hp, selected_chunk

    def get_mana_memory_address(self, expected_mp):
        """
        Get List of tuples, where tuple[0] is a current hp and tuple[1] is a max hp.
        each tuple has inside memory_address and startable value.

        :param expected_hp: your hp. To proper work, your max nad current hp should be equal.
        :return: list of tuples, where list[0] is a max_mp, and list[1] is a current_mp
        """
        int_value = IntValue(int(expected_mp))
        results = []
        selected_chunk = None
        for chunk in self.chunks:
            base_address, _ = chunk
            chunk_bytes = self.memory.get_chunk_bytes(chunk)
            if int_value.bytes in chunk_bytes:
                try:
                    offsets = [m.start() for m in re.finditer(int_value.bytes, chunk_bytes)]
                    if offsets and len(offsets) == 2 and offsets[0] % 8 == 0 and offsets[1] % 8 == 0 and abs(offsets[0] - offsets[1]) == 8:
                        for offset in offsets:
                            selected_chunk = chunk
                            results.append((hex(base_address + offset), int_value.value))
                        break
                except re.error as ex:
                    LOGGER.debug("Could not scan chunks for mana. Exception %s", ex)
        max_mp, current_mp = results[0], results[1]
        return max_mp, current_mp, selected_chunk
