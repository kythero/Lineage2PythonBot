import Pyro4


class PyroHelper(object):

    def __init__(self, host=None, port=None):
        self.host = host
        self.port = int(port)

    @property
    def ns(self):
        return Pyro4.locateNS(host=self.host, port=self.port)

    def available(self):
        return self.ns.list()

    def get_server(self, name):
        server = None
        try:
            uri = self.ns.lookup(name)
            server = Pyro4.Proxy(uri)
        except Exception as ex:
            pass
        return server
