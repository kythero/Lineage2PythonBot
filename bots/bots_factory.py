from bots.characters.cardinal import Cardinal
from bots.characters.player import Player
from bots.characters.fortune_seeker import FortuneSeeker
from bots.characters.summoner import Summoner

class BotsFactory(object):

    BOTS = {
        'cardinal': Cardinal,
        'player': Player,
        'spoiler': FortuneSeeker,
        'summoner': Summoner
    }

    @classmethod
    def get_instance(cls, class_name):
        bot = cls.BOTS[class_name]()
        return bot
