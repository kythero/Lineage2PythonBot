import logging
import time
from helpers.helper import Helper
from .bots_factory import BotsFactory
from utils.EventController.advanced_controller import AdvancedController
from utils.logs.table_logger import TableLogger

LOGGER = logging.getLogger(__name__)


class CreateBot(object):
    """
    Class which create bots. Include process with console parameters.
    """

    def __init__(self, builder):
        self.bot = BotsFactory.get_instance(builder.bot_class)
        self.bot.client = builder.client
        self.bot.name = builder.character_name
        self.pid = builder.pid
        self.bot.main_target = builder.main_target
        self.bot.pid = builder.pid
        self.bot.max_hp = builder.max_hp
        self.bot.max_mp = builder.max_mp
        self.bot.service = builder.service
        self.bot.auto_potions = builder.auto_potions
        self.bot.set_controller(builder.controller)

    def start(self):
        if self.bot.controller:
            self.bot.send('/target {}'.format(self.bot.name))
            LOGGER.info("Request for self-target, using controller: %s", self.bot.controller.__class__.__name__)
        else:
            LOGGER.warning("Controller is not defined! Request for self-target failed.")
        time.sleep(1)
        self.bot.validate_memory()
        LOGGER.info("CLIENT IS READY!")
        self.bot.start()


    @classmethod
    def builder(cls, client, file_name):
        return cls._Builder(client, file_name)

    class _Builder(object):

        def __init__(self, client, file_name):
            self._client = client
            self.file_name = file_name
            self.stored_setup = dict()
            self.pid = None
            self.bot_class = None
            self.process_name = None
            self.character_name = None
            self.max_hp = None
            self.max_mp = None
            self.main_target = None
            self.service = None
            self.controller = None
            self.auto_potions = None

        @property
        def client(self):
            return self._client

        def _process_name(self):
            response = Helper.console().insert("process_name", self.stored_setup,
                                               message="Write title of your Lineage 2 window: ")
            return response

        def _process_name_old(self):
            response = Helper.console().insert("process_name", self.stored_setup,
                                               message="Select process name (l2.exe, l2.bin) or write already existing PID.")
            return response

        def _bot_class(self):
            registered_bots = ', '.join(BotsFactory.BOTS.keys())
            response = Helper.console().insert("class", self.stored_setup,
                                               message="Select registered class (%s)." % (registered_bots,))
            return response

        def _character_name(self):
            response = Helper.console().insert("character_name", self.stored_setup,
                                               message="Write your bot-character's name.")
            return response

        def _max_hp(self):
            response = Helper.console().insert("max_hp", self.stored_setup,
                                               message="Write your bot-character maximum HP and be sure it's fully recovered.")
            return response

        def _max_mp(self):
            response = Helper.console().insert("max_mp", self.stored_setup,
                                               message="Write your bot-character maximum MP and be sure it's fully recovered.")
            return response

        def _main_target(self):
            response = Helper.console().insert("main_target", self.stored_setup,
                                               message="Select a parent for this bot. Bot will assist/follow it. Write character's name.")
            return response

        def _service(self):
            response = Helper.console().insert("service", self.stored_setup,
                                               message="Is service running (yes/no)")
            return response

        def _auto_potions(self):
            response = Helper.console().insert("auto_potions", self.stored_setup,
                                               message="Do you want start auto-potion (yes/no)")
            return response

        def summary(self):
            table_logger = TableLogger("Summary configuration for: {}".format(self.file_name),
                                       "class", "character's name", "expected hp", "expected mp",
                                       "follower/assister", "running service", "auto potions")
            table_logger.add_row(self.bot_class, self.character_name, self.max_hp, self.max_mp,
                                 self.main_target, self.service, self.auto_potions)
            return table_logger.get_table()

        def build(self):
            LOGGER.info("Builder started.")
            time.sleep(0.3)
            self.stored_setup = Helper.file().load(self.file_name) or dict()

            # TODO hardcoded only advancedController. Must be a choice!
            title = self._process_name() or ""
            available_hwnds = AdvancedController.get_hwnd(title)
            if len(available_hwnds) > 1:
                table_logger = TableLogger("Found {} pids.".format(len(available_hwnds)), "option", "pid", "title",
                                           "created time")
                for index, hwnd in enumerate(available_hwnds):
                    table_logger.add_row("[{}]".format(index), hwnd['pid'], hwnd['title'], hwnd['time'])
                table_logger.sort_by("created time")
                table_logger.add_footer("Pick an option to set proper process for your script.")
                LOGGER.info(table_logger.get_table())
                # TODO Validations!
                option = input("Select option: ") or None
                hwnd = available_hwnds[int(option)]
                LOGGER.info(hwnd)
                self.controller = AdvancedController(hwnd['hwnd'])
                self.pid = hwnd['pid']
            elif len(available_hwnds) == 1:
                self.controller = AdvancedController(available_hwnds[0]['hwnd'])
                self.pid = available_hwnds[0]['pid']
            else:
                raise ValueError("Did not find any possible hwnds! Exit.")

            self.bot_class = self._bot_class()
            self.character_name = self._character_name()
            self.max_hp = self._max_hp()
            self.max_mp = self._max_mp()
            self.main_target = self._main_target()
            self.service = self._service()
            self.auto_potions = self._auto_potions()

            Helper.file().save(self.file_name, self.stored_setup)

            LOGGER.info(self.summary())

            return CreateBot(self)
