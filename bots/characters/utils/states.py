

class MODES(object):
    IDLE, TARGETING, HEALING, SPOILING, SWEEPING, ATTACKING, ASSISTING = range(7)


class Points(object):

    def __init__(self, current, max):
        self.current = current
        self.max = max


class Target(object):

    def __init__(self, name, current_hp, max_hp):
        self.name = name
        self.hp = Points(current_hp, max_hp)

    def reset(self):
        self.name = None
        self.hp.current = 0
        self.hp.max = 0
