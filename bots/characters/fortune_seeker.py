import logging

from utils.constants import MAX_HP, VALUE
from utils.expectancy.core.exceptions import TimeoutError
from utils.expectancy.expectancy import Expectancy
from utils.logs.table_logger import TableLogger
from .abstract.bot import Bot
from .utils.states import MODES, Target
import time

LOGGER = logging.getLogger(__name__)


class FortuneSeeker(Bot):

    _COMMAND_SPOIL = "/useskill spoil"
    _COMMAND_SWEEPER = "/useskill sweeper"

    INTERVAL_FOLLOW = 7
    INTERVAL_ATTACK = 4
    UPLOAD_INTERVAL = 0.3
    SWEEP_DELAY = 1
    MONSTER = 'monster'

    def __init__(self):
        super(FortuneSeeker, self).__init__()
        self.target = Target(name=None, current_hp=0, max_hp=0)
        self.mode = MODES.IDLE
        self.spoil = False

    def start(self):
        Expectancy.register(func=self.do_follow, name="follow", interval=self.INTERVAL_FOLLOW)
        Expectancy.register(func=self.do_attack, name="attack", interval=self.INTERVAL_ATTACK)
        Expectancy.register(func=self.client.sgi.upload_data, name="upload", args=(self.client.id, self.storage),
                            interval=self.UPLOAD_INTERVAL)
        Expectancy.register(func=self.show_storage, name="show", interval=3)
        Expectancy.register(func=self.show_client_data, name="show_client", interval=4)
        self.send(self.command_social_dance)
        while self.running:
            if self.client.STATUS:
                Expectancy.execute(name="upload")
                Expectancy.execute(name="show")
                Expectancy.execute(name="show_client")
            self.update()

    def show_guardian(self):
        table = TableLogger("Spoiler - assister data", "Assister: {}".format(self.main_target), "value")
        table.add_row("assister target max HP", self.guardian.target_hp.max)
        table.add_row("assister target current HP", self.guardian.target_hp.current)
        LOGGER.info(self.client.DATA)
        LOGGER.info(table.get_table())

    def do_target_reason(self):
        self.reload_data()
        if self.target_hp.max == self.guardian.hp.max:
            self.target.name = self.guardian.name
            self.target.hp.max = self.guardian.hp.max
            self.target.hp.current = self.guardian.hp.current
            return True
        return False

    def do_attack(self):
        if not self.spoil:
            self.do_spoil()
        else:
            self.command_attack()

    def do_spoil(self):
        self.send(self._COMMAND_SPOIL)
        self.spoil = True

    def do_sweeper(self):
        self.send(self._COMMAND_SWEEPER)
        time.sleep(self.SWEEP_DELAY)

    def do_target(self):
        self.command_target(self.guardian.name)

    def do_assist_reason(self):
        """
        Wait until spoiler's new target will have different max_hp than saved one in self.target
        and the new target will be the same like his guardian has.

        :return: True if success, False otherwise.
        """
        self.reload_data()
        if self.target.hp.max != self.target_hp.max and self.target_hp.max == self.guardian.target_hp.max:
            return True
        return False

    def do_follow(self):
        self.reload_data()
        if self.target.name != self.guardian.name and self.target_hp.max != self.guardian.hp.max:
            try:
                Expectancy.do(func=self.do_target).wait(func=self.do_target_reason)
            except TimeoutError as ex:
                LOGGER.info(ex)
        self.do_target()

    def get_mode(self):
        all_hps = [self.hp.max]
        for user, values in self.client.DATA.items():
            try:
                all_hps.append(values[MAX_HP][VALUE])
            except KeyError as ex:
                LOGGER.error("Could not retrieve max_hp for user %s. Exception %s", user, ex)

        mode = MODES.IDLE
        if self.mode == MODES.ATTACKING:
            if self.target_hp.max > 0 and self.target_hp.current > 0:
                mode = MODES.ATTACKING
            else:
                self.do_sweeper()
                self.reset_target()
                mode = MODES.IDLE
        else:
            # if spoil has something selected and his guardian did not select anyone from clients, and the guardian's selection alive - assist
            if self.target.name and self.guardian.target_hp.max not in all_hps and self.guardian.target_hp.max > 0 and self.guardian.target_hp.current > 0:
                mode = MODES.ASSISTING

        return mode

    def update(self):
        self.reload_data()
        self.mode = self.get_mode()

        if self.mode == MODES.IDLE:
            response = Expectancy.execute(name="follow")
            if response:
                self.target.name = response
                self.target.hp.max = self.target_hp.max
                self.target.hp.current = self.target_hp.current
        elif self.mode == MODES.ASSISTING:
            try:
                Expectancy.do(func=self.do_target).wait(func=self.do_target_reason)
                Expectancy.do(func=self.command_assist).wait(func=self.do_assist_reason)
                self.target.name = self.MONSTER
                self.target.hp.max = self.target_hp.max
                self.target.hp.current = self.target_hp.current
                self.mode = MODES.ATTACKING
                Expectancy.execute(name="attack")
            except TimeoutError as ex:
                self.mode = MODES.IDLE
                LOGGER.info(ex)
        elif self.mode == MODES.ATTACKING:
            Expectancy.execute(name="attack")

    def reset_target(self):
        self.target.reset()
        self.spoil = False
