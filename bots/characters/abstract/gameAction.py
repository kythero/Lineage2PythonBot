from .action import Action
import logging
import time

LOGGER = logging.getLogger(__name__)


class GameAction(Action):

    _COMMAND_TARGET = "/target {user}"
    _COMMAND_INVITE = "/invite {user}"
    _COMMAND_TRADE = "/trade"
    _COMMAND_ASSIST = "/assist"
    _COMMAND_ATTACK = "/attack"
    _COMMAND_FOLLOW = _COMMAND_TARGET
    _COMMAND_RESET_CAMERA = "{HOME}"

    _COMMAND_DELAY = 0.3

    _COMMAND_SOCIAL_DANCE = "/socialdance"

    def __init__(self):
        super(GameAction, self).__init__()
        self.targeted = None

    def command_target(self, user):
        LOGGER.debug("Trying target the user: %s", user)
        self.send(self._COMMAND_TARGET.format(user=user))
        return user

    def command_invite(self, user):
        LOGGER.debug("Send invitation to party to: %s", user)
        self.send(self._COMMAND_INVITE.format(user=user))
        time.sleep(self._COMMAND_DELAY)

    def command_trade(self, user):
        self.command_target(user)
        LOGGER.debug("Trading with: %s.", user)
        self.send(self._COMMAND_TRADE)
        time.sleep(self._COMMAND_DELAY)

    def command_assist(self):
        LOGGER.debug("Assisting.")
        self.send(self._COMMAND_ASSIST)
        time.sleep(self._COMMAND_DELAY)

    def command_attack(self):
        LOGGER.debug("Attacking.")
        self.send(self._COMMAND_ATTACK)
        time.sleep(self._COMMAND_DELAY)

    def command_follow(self, user):
        self.command_target(user)
        #self.send(self._COMMAND_RESET_CAMERA)
        return user

    def command_social_dance(self):
        self.send(self._COMMAND_SOCIAL_DANCE)