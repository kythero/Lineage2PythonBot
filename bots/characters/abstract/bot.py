import logging
from abc import ABC, abstractmethod
from collections import namedtuple

from helpers.helper import Helper
from utils.constants import (
    CURRENT_HP,
    CURRENT_MP,
    CURRENT_TARGET_HP,
    MAX_HP,
    MAX_MP,
    MAX_TARGET_HP,
    MEMORY_ADDRESS,
    VALUE
)
from utils.logs.table_logger import TableLogger
from utils.memoryScanner.ms import MemoryScanner
from utils.wrappers import wrappers
from .gameAction import GameAction

LOGGER = logging.getLogger(__name__)

Entity = namedtuple('Entity', ('current', 'max', 'memory_current', 'memory_max'))
GUARDIAN = namedtuple("Guardian", ("name", "target_hp", "hp"))
ENTITY2 = namedtuple("Entity", ("current", "max"))


class Bot(GameAction, ABC):
    """
    Abstract class
    """

    def __init__(self):
        super(Bot, self).__init__()
        self._class = None
        self.client = None
        self.name = None
        self.main_target = ""
        self.pid = None
        self.max_hp = None
        self.max_mp = None
        self.service = None
        self.auto_potions = None
        self.storage = None

        self.running = True
        self.auto_potion_invoke_percent = 75

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def update(self):
        pass

    def show_client_data(self):
        table = TableLogger("Client DATA", "client", "currentMP/maxHP", "currentMP/maxMP", "currentTargetHP/maxTargetHP")
        for client, values in self.client.DATA.items():
            try:
                table.add_row(
                    client,
                    "{}/{}".format(values[CURRENT_HP][VALUE], values[MAX_HP][VALUE]),
                    "{}/{}".format(values[CURRENT_MP][VALUE], values[MAX_MP][VALUE]),
                    "{}/{}".format(values[CURRENT_TARGET_HP][VALUE], values[MAX_TARGET_HP][VALUE]),
                )
            except KeyError:
                table.add_row(client, "N/A", "N/A", "N/A")
        LOGGER.info(table.get_table())

    def show_storage(self):
        table = TableLogger("Player storage data", "kind", "current (memory)", "max (memory)")
        table.add_row("HP", "{} ({})".format(self.hp.current, self.hp.memory_current), "{} ({})".format(self.hp.max, self.hp.memory_max))
        table.add_row("MP", "{} ({})".format(self.mp.current, self.mp.memory_current), "{} ({})".format(self.mp.max, self.mp.memory_max))
        table.add_row("TARGET HP", "{} ({})".format(self.target_hp.current, self.target_hp.memory_current), "{} ({})".format(self.target_hp.max, self.target_hp.memory_max))
        LOGGER.info(table.get_table())

    @property
    def guardian(self):
        try:
            _target_hp = ENTITY2(
                self.client.DATA[self.main_target][CURRENT_TARGET_HP][VALUE],
                self.client.DATA[self.main_target][MAX_TARGET_HP][VALUE]
            )
        except KeyError:
            _target_hp = ENTITY2(0, 0)

        try:
            _guardian_hp = ENTITY2(
                self.client.DATA[self.main_target][CURRENT_HP][VALUE],
                self.client.DATA[self.main_target][MAX_HP][VALUE]
            )
        except KeyError:
            _guardian_hp = ENTITY2(0, 0)

        return GUARDIAN(self.main_target, _target_hp, _guardian_hp)

    @property
    @wrappers.default_value(value=Entity(0, 0, 0, 0), expected_exceptions=KeyError)
    def target_hp(self):
        return Entity(
            self.storage[self.name][CURRENT_TARGET_HP][VALUE],
            self.storage[self.name][MAX_TARGET_HP][VALUE],
            self.storage[self.name][CURRENT_TARGET_HP][MEMORY_ADDRESS],
            self.storage[self.name][MAX_TARGET_HP][MEMORY_ADDRESS]
        )

    @property
    @wrappers.default_value(value=Entity(0, 0, 0, 0), expected_exceptions=KeyError)
    def hp(self):
        return Entity(
            self.storage[self.name][CURRENT_HP][VALUE],
            self.storage[self.name][MAX_HP][VALUE],
            self.storage[self.name][CURRENT_HP][MEMORY_ADDRESS],
            self.storage[self.name][MAX_HP][MEMORY_ADDRESS]
        )

    @property
    @wrappers.default_value(value=Entity(0, 0, 0, 0), expected_exceptions=KeyError)
    def mp(self):
        return Entity(
            self.storage[self.name][CURRENT_MP][VALUE],
            self.storage[self.name][MAX_MP][VALUE],
            self.storage[self.name][CURRENT_MP][MEMORY_ADDRESS],
            self.storage[self.name][MAX_MP][MEMORY_ADDRESS]
        )

    def reload_data(self):
        """
        Rescan memory but for given memory addresses. It updates self.storage with new values, the bot's data.
        Method does not throw any exception. Here we only check if data reloaded fully, or partial.

        :return: True if data fully reloaded, False if partial.
        """
        result = True
        with MemoryScanner(self.pid) as memory:
            # TODO It works but need refactor. Can use values from iteration, or keep naming for clear solution.
            for key, values in self.storage.items():
                for value, _ in values.items():
                    try:
                        self.storage[key][value][VALUE] = memory.read(self.storage[key][value][MEMORY_ADDRESS])
                    except KeyError as ex:
                        LOGGER.debug("An issue occured during iteration for "
                                     "reload_data() with parameters: %s, %s. Exception: %s", key, value, ex)
                        result = False
        return result

    def validate_memory(self):
        LOGGER.info("Starting memory validation for character: [%s].", self.name)

        data = dict()
        LOGGER.info("Validating memory for pid: %s", self.pid)
        hp_chunk = None
        mp_chunk = None
        target_chunk = None
        with MemoryScanner(self.pid) as memory:
            chunks = memory.get_chunks()
            try:
                max_hp, current_hp, hp_chunk = Helper.memory(memory, chunks).get_health_memory_address(self.max_hp)
                LOGGER.info("HP chunk: %s", hp_chunk)
                data[MAX_HP] = {
                    MEMORY_ADDRESS: max_hp[0],
                    VALUE: max_hp[1]
                }
                data[CURRENT_HP] = {
                    MEMORY_ADDRESS: current_hp[0],
                    VALUE: current_hp[1]
                }
            except IndexError:
                LOGGER.info("Could not get your current and max HP.")

            try:
                max_mp, current_mp, mp_chunk = Helper.memory(memory, chunks).get_mana_memory_address(self.max_mp)
                LOGGER.info("mp chunk: %s", mp_chunk)
                data[MAX_MP] = {
                    MEMORY_ADDRESS: max_mp[0],
                    VALUE: max_mp[1]
                }
                data[CURRENT_MP] = {
                    MEMORY_ADDRESS: current_mp[0],
                    VALUE: current_mp[1]
                }
            except IndexError:
                LOGGER.info("Could not get your current and max MP.")

            try:
                max_target_hp, current_target_hp, target_chunk = Helper.memory(memory, chunks).get_target_health_memory_address(self.max_hp, choosen_chunk=hp_chunk)
                LOGGER.info("target chunk: %s", target_chunk)
                data[MAX_TARGET_HP] = {
                    MEMORY_ADDRESS: max_target_hp[0],
                    VALUE: max_target_hp[1]
                }
                data[CURRENT_TARGET_HP] = {
                    MEMORY_ADDRESS: current_target_hp[0],
                    VALUE: current_target_hp[1]
                }
            except IndexError:
                LOGGER.info("Could not get current and max target HP.")
    #
        self.storage = {
            self.name: data
        }
        LOGGER.debug("Collected data: %s", self.storage)
        return self
