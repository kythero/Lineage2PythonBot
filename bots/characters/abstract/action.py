from utils.EventController.controller import Controller
from utils.EventController.advanced_controller import AdvancedController

class Action(object):

    def __init__(self, advanced_controller=False, hwnd=None):
        if advanced_controller:
            self.controller = AdvancedController(hwnd)
        else:
            self.controller = Controller()

    def press_key(self, hex_key_code):
        self.controller.press_key(hex_key_code)

    def release_key(self, hex_key_code):
        self.controller.release_key(hex_key_code)

    def send_key(self, hex_key_code):
        self.controller.send_key(hex_key_code)

    def send(self, message):
        self.controller.send(message)

    def set_controller(self, controller):
        self.controller = controller
