import logging

from utils.constants import MAX_HP, CURRENT_HP, VALUE
from utils.expectancy.expectancy import Expectancy
from .abstract.bot import Bot
from .utils.states import Target, MODES

LOGGER = logging.getLogger(__name__)


class Summoner(Bot):

    INTERVAL_FOLLOW = 7
    UPLOAD_INTERVAL = 0.3

    def __init__(self):
        super(Summoner, self).__init__()
        self.target = Target(name=None, current_hp=0, max_hp=0)
        self.mode = MODES.IDLE

    def start(self):
        Expectancy.register(func=self.do_follow, name="follow", interval=self.INTERVAL_FOLLOW)
        Expectancy.register(func=self.client.sgi.upload_data, name="upload", args=(self.client.id, self.storage),
                            interval=self.UPLOAD_INTERVAL)
        Expectancy.register(func=self.show_client_data, name="show_client", interval=4)
        while self.running:
            if self.client.STATUS:
                Expectancy.execute(name="upload")
                Expectancy.execute(name="show_client")
            self.update()

    def do_target(self):
        self.command_target(self.guardian.name)

    def do_target_reason(self):
        self.reload_data()
        if self.target_hp.max == self.guardian.hp.max:
            self.target.name = self.guardian.name
            self.target.hp.max = self.guardian.hp.max
            self.target.hp.current = self.guardian.hp.current
            return True
        return False

    def select_user(self, user):
        self.command_target(user)

    def select_user_reason(self, user):
        self.reload_data()
        try:
            if self.target_hp.max == self.client.DATA[user][MAX_HP][VALUE]:
                self.target.name = user
                self.target.hp.max = self.client.DATA[user][MAX_HP][VALUE]
                self.target.hp.current = self.client.DATA[user][CURRENT_HP][VALUE]
                return True
        except KeyError as ex:
            LOGGER.error("There was a problem with selecting user: %s. Exception %s.", user, ex)
        return False

    def do_follow(self):
        self.reload_data()
        if self.target.name != self.guardian.name and self.target_hp.max != self.guardian.hp.max:
            try:
                Expectancy.do(func=self.do_target).wait(func=self.do_target_reason)
            except TimeoutError as ex:
                LOGGER.info(ex)
        self.do_target()

    def update(self):
        self.reload_data()
        self.mode = self.get_mode()

        if self.mode == MODES.IDLE:
            Expectancy.execute(name="follow")

    def get_mode(self):
        mode = MODES.IDLE
        return mode

    def reset_target(self):
        self.target.reset()