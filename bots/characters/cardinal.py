import logging
from statistics import median, mean

from utils.constants import MAX_HP, CURRENT_HP, VALUE
from utils.expectancy.expectancy import Expectancy
from utils.expectancy.core.exceptions import TimeoutError
from .abstract.bot import Bot
from .utils.states import Target, MODES

LOGGER = logging.getLogger(__name__)


class Cardinal(Bot):
    """
    Cardinal class
    """
    USERS = dict()
    INTERVAL_FOLLOW = 7
    ENGINE_DELAY = 0.5
    PYRO_DELAY = 0.3
    SKILL_BALANCE_LIFE = "/useskill balance life"
    SKILL_MAJOR_GROUP_HEAL = "/useskill major group heal"
    SKILL_GREATER_BATTLE_HEAL = "/useskill greater battle heal"
    SKILL_MAJOR_HEAL = "/useskill major heal"
    HEALING_DELAY = 1.8

    def __init__(self):
        super(Cardinal, self).__init__()
        self.target = Target(name=None, current_hp=0, max_hp=0)
        self.mode = MODES.IDLE

    def start(self):
        Expectancy.register(func=self.unpack_data, name="unpack", interval=self.PYRO_DELAY)
        Expectancy.register(func=self.client.sgi.upload_data, name="upload", args=(self.client.id, self.storage,), interval=1)
        Expectancy.register(func=self.do_follow, name="follow", interval=self.INTERVAL_FOLLOW)
        Expectancy.register(func=self.healing, name="healing", interval=self.HEALING_DELAY)
        self.send(self.command_social_dance)
        while self.running:
            if self.client.STATUS:
                Expectancy.execute(name="unpack")
                Expectancy.execute(name="upload")
            self.update()

    def do_target(self):
        self.command_target(self.guardian.name)

    def select_user(self, user):
        self.command_target(user)

    def select_user_reason(self, user):
        self.reload_data()
        try:
            if self.target_hp.max == self.client.DATA[user][MAX_HP][VALUE]:
                self.target.name = user
                self.target.hp.max = self.client.DATA[user][MAX_HP][VALUE]
                self.target.hp.current = self.client.DATA[user][CURRENT_HP][VALUE]
                return True
        except KeyError as ex:
            LOGGER.error("There was a problem with selecting user: %s. Exception %s.", user, ex)
        return False

    def do_follow(self):
        self.reload_data()
        if self.target.name != self.guardian.name and self.target_hp.max != self.guardian.hp.max:
            try:
                Expectancy.do(func=self.do_target).wait(func=self.do_target_reason)
            except TimeoutError as ex:
                LOGGER.info(ex)
        self.do_target()

    def do_target_reason(self):
        self.reload_data()
        if self.target_hp.max == self.guardian.hp.max:
            self.target.name = self.guardian.name
            self.target.hp.max = self.guardian.hp.max
            self.target.hp.current = self.guardian.hp.current
            return True
        return False

    def update(self):
        self.reload_data()
        self.mode = self.get_mode()

        if self.mode == MODES.IDLE:
            Expectancy.execute(name="follow")
        elif self.mode == MODES.HEALING:
            Expectancy.execute(name="healing")

    def get_mode(self):
        if not self.USERS:
            return MODES.IDLE

        choose = []
        for user, value in self.USERS.items():
            percent = value[CURRENT_HP] * 100 // value[MAX_HP]
            choose.append((user, percent))

        choose.sort(key=lambda x: x[1])

        heal = None
        if self.balance_life(choose) or self.major_group_heal(choose):
            heal = True
        else:
            user, hp = choose[0]
            if hp < 80:
                heal = True

        return MODES.HEALING if heal else MODES.IDLE

    def balance_life(self, hp_list):
        """
        Balance life for cardinal.
        - if in party median is higher than 80% but someone has less than 30% - BALANCE LIFE

        :param hp_list: list of hps from clients (bots).
        :return: useskill string, otherwise False.
        """
        if median([hp for _, hp in hp_list]) > 80 and any(hp < 30 for _, hp in hp_list):
            LOGGER.debug("Median is higher than 80 and someone has less than 30% hp.")
            skill = self.SKILL_BALANCE_LIFE
        else:
            skill = False
        return skill

    def major_group_heal(self, hp_list):
        """
        Major Group Heal for cardinal.
        - if in party at least 3 users have HP less than 80% - MAJOR GROUP HEAL
        - if in party at least 2 users have HP less than 75% - MAJOR GROUP HEAL
        - if in party with more than 2 people, average of current HP is less than 80% - MAJOR GROUP HEAL

        :param hp_list: list of hps from clients (bots).
        :return: useskill string, otherwise False.
        """
        if ((len(hp_list) >= 3 and [hp < 80 for _, hp in hp_list].count(True) >= 3) or
            (len(hp_list) > 2 and mean([hp for _, hp in hp_list]) < 80) or
            (len(hp_list) >= 2 and [hp < 75 for _, hp in hp_list].count(True) >= 2)):
            skill = self.SKILL_MAJOR_GROUP_HEAL
        else:
            skill = False
        return skill

    def healing(self):
        """
        Main Logic. Bishop's actions are:
        - if in party median is higher than 80% but someone has less than 30% - BALANCE HEAL
        - if in party at least 3 users have HP less than 80% - MAJOR GROUP HEAL
        - if in party at least 2 users have HP less than 75% - MAJOR GROUP HEAL
        - if in party with more than 2 people, average of current HP is less than 80% - MAJOR GROUP HEAL
        - if in party only one user has less than 50% HP - /target <user> - GREATER BATTLE HEAL
        - if in party only one user has less than 80% HP - /target <user> - MAJOR HEAL
        """
        if not self.USERS:
            return False

        choose = []
        for user, value in self.USERS.items():
            percent = value[CURRENT_HP] * 100 // value[MAX_HP]
            choose.append((user, percent))

        choose.sort(key=lambda x: x[1])

        if median([hp for _, hp in choose]) > 80 and any(hp < 30 for _, hp in choose):
            LOGGER.debug("Median is higher than 80 and someone has less than 30% hp.")
            self.send(self.SKILL_BALANCE_LIFE)

        elif len(choose) >= 3 and [hp < 80 for _, hp in choose].count(True) >= 3:
            LOGGER.debug("Party has more than 2 people and more than 3 have less than 80% hp.")
            self.send(self.SKILL_MAJOR_GROUP_HEAL)

        elif len(choose) > 2 and mean([hp for _, hp in choose]) < 80:
            LOGGER.debug("Party has more than 2 members and average of HP is 80%.")
            self.send(self.SKILL_MAJOR_GROUP_HEAL)

        elif len(choose) >= 2 and [hp < 75 for _, hp in choose].count(True) >= 2:
            LOGGER.debug("Party has more than 1 member and 2 or more of their HP are less than 75%")
            self.send(self.SKILL_MAJOR_GROUP_HEAL)

        else:
            user, hp = choose[0]
            if hp < 50:
                # TODO check user using memory if its really selected!
                if self.target.name == user:
                    self.mode = MODES.HEALING
                    self.send(self.SKILL_GREATER_BATTLE_HEAL)
                else:
                    try:
                        Expectancy.do(func=self.select_user, args=(user,)).wait(func=self.select_user_reason, args=(user,))
                        self.mode = MODES.HEALING
                        self.send(self.SKILL_GREATER_BATTLE_HEAL)
                    except TimeoutError as ex:
                        LOGGER.info(ex)

            elif hp < 80:
                # TODO check user using memory if its really selected!
                if self.target.name == user:
                    self.mode = MODES.HEALING
                    self.send(self.SKILL_MAJOR_HEAL)
                else:
                    try:
                        Expectancy.do(func=self.select_user, args=(user,)).wait(func=self.select_user_reason,
                                                                                args=(user,))
                        self.mode = MODES.HEALING
                        self.send(self.SKILL_MAJOR_HEAL)
                    except TimeoutError as ex:
                        LOGGER.info(ex)

    def unpack_data(self):
        """
        Unpack data stored inside client. Client data is all the time updated by server.

        :return: True if unpacked everything. False in partial case.
        """
        result = True
        for user in self.client.DATA.keys():
            try:
                data = {
                    user: {
                        CURRENT_HP: self.client.DATA[user][CURRENT_HP][VALUE],
                        MAX_HP: self.client.DATA[user][MAX_HP][VALUE]
                    }
                }
                self.USERS.update(data)
            except KeyError as ex:
                result = False
                LOGGER.error("There was a problem with unpack data for user %s. Exception %s", user, ex)
        return result

    def reset_target(self):
        self.target.reset()
