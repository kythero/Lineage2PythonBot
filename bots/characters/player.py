import logging
import time

from bots.characters.abstract.bot import Bot
from utils.expectancy.expectancy import Expectancy

LOGGER = logging.getLogger(__name__)


class Player(Bot):
    """
    Class for Player
    """
    UPLOAD_INTERVAL = 0.3
    LOOP_DELAY = 0.1

    def __init__(self):
        super(Player, self).__init__()

    def start(self):
        Expectancy.register(func=self.client.sgi.upload_data, name="upload", args=(self.client.id, self.storage), interval=self.UPLOAD_INTERVAL)
        Expectancy.register(func=self.show_storage, name="show", interval=3)
        self.send(self.command_social_dance)
        while self.running:
            self.update()
            Expectancy.execute(name="upload")
            time.sleep(self.LOOP_DELAY)

    def update(self):
        self.reload_data()
        Expectancy.execute(name="show")
