#!/usr/bin/python3
import logging
from utils.name_server import NameServer
from utils.sgi import ServerGatewayInterface
from utils.server import Server
from helpers.helper import Helper

logging.basicConfig(
    level=logging.DEBUG,
    format="[%(levelname)s][%(asctime)s][%(filename)s][%(funcName)s:%(lineno)d]: %(message)s"
)
LOGGER = logging.getLogger(__name__)


class ServerExecutor(object):

    def __init__(self, builder):
        self.name = builder.name
        self.local_host = builder.local_host
        self.local_port = builder.local_port

    def start(self):
        with NameServer(host=self.local_host, port=self.local_port) as name_server:
            with Server(name=self.name) as server:
                sgi = ServerGatewayInterface(name_server=name_server, server=server)
                sgi.start()

    @classmethod
    def builder(cls):
        return cls._Builder()

    class _Builder(object):

        def __init__(self):
            self.name = None
            self.local_host = None
            self.local_port = None

        def _name(self):
            default = "server"
            msg = "Write your server's name [{}]: ".format(default)
            return input(msg) or default

        def _local_host(self):
            default = Helper.ip().get_ip_address()
            msg = "Write your public ip for connection [{}]: ".format(default)
            return input(msg) or default

        def _local_port(self):
            default = 9091
            msg = "Write port for your Pyro4 object [{}]: ".format(default)
            return input(msg) or default

        def build(self):
            self.name = self._name()
            self.local_host = self._local_host()
            self.local_port = int(self._local_port())

            return ServerExecutor(self)

if __name__ == '__main__':
    LOGGER.info("Starting server!")
    instance = ServerExecutor.builder().build()
    instance.start()
